<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

    $sheep = new Hewan("shaun");

        echo "Name : $sheep->name <br>"; // "shaun"
        echo "Legs : $sheep->legs <br>"; // 2
        echo "Cold blooded : $sheep->cold_blooded <br> <br>"; 

        // NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

    $kodok = new Frog("buduk");

        echo "Name : $kodok->name <br>";
        echo "Legs : $kodok->legs <br>";
        echo "Cold blooded : $kodok->cold_blooded <br>"; 
        $kodok->jump();
        echo "<br> <br>";

     $sungkong = new Ape("kera sakit");

        echo "Name : $sungkong->name <br>";
        echo "Legs : $sungkong->legs <br>";
        echo "Cold blooded : $sungkong->cold_blooded <br>"; 
        $sungkong->yell();
?>

